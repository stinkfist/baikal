﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace Croco.Utils
{
	public class GameConfig : MonoBehaviour
	{
		#region Singleton stuff
		public static GameConfig Instance
		{
			get { return _instance; }
		}

		private static GameConfig _instance;

		void Awake()
		{
			if (_instance == null)
			{
				_instance = this;
				DontDestroyOnLoad(gameObject);
			}
			else
			{
				UnityEngine.Debug.LogError("Attempting to instantiate a second GameConfig. Destroying second instance.", gameObject);
				Destroy(gameObject);
				return;
			}

			// Testing values
			_configs.Add("testBool", true);
			_configs.Add("testString", "asdasd");
			_configs.Add("testInt", 17);
		}
		#endregion

		private Dictionary<string, object> _configs = new Dictionary<string, object>();

		public void Set(string key, object value)
		{
			if (!ContainsKey(key))
				_configs.Add(key, value);
			else
				_configs[key] = value;
		}

		public void Remove(string key)
		{
			_configs.Remove(key);
		}

		public T Get<T>(string key, T defaultValue)
		{
			object result;
			if (_configs.TryGetValue(key, out result))
				return (T)_configs[key];
			
			return defaultValue;
		}

		public bool ContainsKey(string key)
		{
			return _configs.ContainsKey(key);
		}

		public IEnumerator GetEnumerator()
		{
			return _configs.GetEnumerator();
		}

		public void Clear()
		{
			_configs.Clear();
		}
	}
}