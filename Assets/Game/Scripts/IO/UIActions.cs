﻿using InControl;

namespace Totem.Game.IO
{
	public class UIActions : PlayerActionSet
	{
		public PlayerAction Up { get; private set; }
		public PlayerAction Down { get; private set; }
		public PlayerAction Left { get; private set; }
		public PlayerAction Right { get; private set; }
	
		public PlayerTwoAxisAction Move { get; private set; }

		public PlayerAction Confirm { get; private set; }
		public PlayerAction Back { get; private set; }
		public PlayerAction Pause { get; private set; }

		public UIActions()
		{
			Up = CreatePlayerAction("Move Up");
			Down = CreatePlayerAction("Move Down");
			Left = CreatePlayerAction("Move Left");
			Right = CreatePlayerAction("Move Right");
			Move = CreateTwoAxisPlayerAction(Left, Right, Down, Up);

			Confirm = CreatePlayerAction("Confirm");
			Back = CreatePlayerAction("Back");
			Pause = CreatePlayerAction("Start");

			SetDefaultValues();
		}

		public void SetDefaultValues()
		{
			Up.ClearBindings();
			Up.AddDefaultBinding(InputControlType.LeftStickUp);
			Up.AddDefaultBinding(InputControlType.DPadUp);
			Up.AddDefaultBinding(Key.W);
			Up.AddDefaultBinding(Key.UpArrow);

			Down.ClearBindings();
			Down.AddDefaultBinding(InputControlType.LeftStickDown);
			Down.AddDefaultBinding(InputControlType.DPadDown);
			Down.AddDefaultBinding(Key.S);
			Down.AddDefaultBinding(Key.DownArrow);

			Left.ClearBindings();
			Left.AddDefaultBinding(InputControlType.LeftStickLeft);
			Left.AddDefaultBinding(InputControlType.DPadLeft);
			Left.AddDefaultBinding(Key.A);
			Left.AddDefaultBinding(Key.UpArrow);

			Right.ClearBindings();
			Right.AddDefaultBinding(InputControlType.LeftStickRight);
			Right.AddDefaultBinding(InputControlType.DPadRight);
			Right.AddDefaultBinding(Key.D);
			Right.AddDefaultBinding(Key.RightArrow);

			Confirm.ClearBindings();
			Confirm.AddDefaultBinding(InputControlType.Action1);
			Confirm.AddDefaultBinding(Key.F);
			Confirm.AddDefaultBinding(Key.P);

			Back.ClearBindings();
			Back.AddDefaultBinding(InputControlType.Action2);
			Back.AddDefaultBinding(Key.G);
			Back.AddDefaultBinding(Key.O);
			Back.AddDefaultBinding(Key.Escape);

			Pause.ClearBindings();
			Pause.AddDefaultBinding(InputControlType.Start);
			Pause.AddDefaultBinding(Key.Return);
			Pause.AddDefaultBinding(Key.Escape);
		}
	}
}