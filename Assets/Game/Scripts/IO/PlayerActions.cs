﻿using InControl;

public class PlayerActions : PlayerActionSet
{
	public PlayerAction Left { get; private set; }
	public PlayerAction Right { get; private set; }
	public PlayerOneAxisAction Move { get; private set; }

	public PlayerAction Jump { get; private set; }
	public PlayerAction Attack { get; private set; }

    public PlayerActions(int gamepadNumber)
	{
		Left = CreatePlayerAction("Move Left");
		Right = CreatePlayerAction("Move Right");
		Move = CreateOneAxisPlayerAction(Left, Right);

		Jump = CreatePlayerAction("Jump");
        Attack = CreatePlayerAction("Attack");

        SetDefaultValues();
	}

	public void SetDefaultValues()
	{
		Left.ClearBindings();
		Left.AddDefaultBinding(InputControlType.LeftStickLeft);
		Left.AddDefaultBinding(InputControlType.DPadLeft);
		Left.AddDefaultBinding(Key.LeftArrow);

		Right.ClearBindings();
		Right.AddDefaultBinding(InputControlType.LeftStickRight);
		Right.AddDefaultBinding(InputControlType.DPadRight);
		Right.AddDefaultBinding(Key.RightArrow);

		Jump.ClearBindings();
		Jump.AddDefaultBinding(InputControlType.Action1);
		Jump.AddDefaultBinding(Key.C);
		Jump.AddDefaultBinding(Key.UpArrow);

        Attack.ClearBindings();
        Attack.AddDefaultBinding(InputControlType.Action3);
        Attack.AddDefaultBinding(Key.X);
	}
}