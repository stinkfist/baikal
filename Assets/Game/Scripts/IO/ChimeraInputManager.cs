﻿using UnityEngine;
using InControl;
using System.Collections;

public class ChimeraInputManager
{
    private Chimera _chimera;
    private InputDevice _device;
    private PlayerActions _actions;

    #region Input Values
    public float HorizontalAxisValue
    {
        get
        {
            return _actions.Move;
        }
    }

    public float RawHorizontalAxisValue { get { return _actions.Move; } }

    public bool JumpButtonPressed { get { return _actions.Jump.IsPressed; } }
    public bool JumpButtonJustPressed { get { return JumpButtonPressed && _actions.Jump.HasChanged; } }
    public bool JumpButtonJustReleased { get { return !JumpButtonPressed && _actions.Jump.HasChanged; } }

    public bool AttackButtonPressed { get { return _actions.Attack.IsPressed; } }
    public bool AttackButtonJustPressed { get { return AttackButtonPressed && _actions.Attack.HasChanged; } }
    public bool AttackButtonJustReleased { get { return !AttackButtonPressed && _actions.Attack.HasChanged; } }

    #endregion

    public ChimeraInputManager(Chimera c)
    {
        _chimera = c;

        _actions = new PlayerActions(1);

        /*
        foreach (InputDevice device in InputManager.Devices)
        {
            if (device.IsAttached)
            {
                _device = device;
            }

        }

        _actions.Device = _device;
        */
    }

    public void Vibrate(float intensity, float duration)
    {
        if (duration <= 0 || intensity <= 0) return;

        _chimera.StartCoroutine(MakeGamepadVibrate(intensity, duration));
    }

    private IEnumerator MakeGamepadVibrate(float intensity, float duration)
    {
        _device.Vibrate(intensity);
        yield return new WaitForSeconds(duration);
        _device.StopVibration();
    }
}
