﻿using UnityEngine;

public class Chimera : MonoBehaviour
{
    [SerializeField]
    [Range(1, 2)]
    private int _playerNumber;
    public int PlayerNumber { get { return _playerNumber; } }

    [SerializeField]
    private MovementModule _movementModulePrefab;
    private MovementModule _movementModule;

    [SerializeField]
    private AttackModule _attackModulePrefab;
    private AttackModule _attackModule;

    private ChimeraInputManager _inputManager;
    public ChimeraInputManager InputManager { get { return _inputManager; } }

    [SerializeField]
    private BoxCollider2D _feetCollider;
    public BoxCollider2D BottomCollider { get { return _feetCollider; } }

    public bool IsFacingRight;
    public bool CanMove = true;

    private void Start()
    {
        //_healthModule.SetChimera(this);
        _movementModule = Instantiate(_movementModulePrefab, transform.Find("Modules/Movement"));
        _movementModule.SetChimera(this);

        _attackModule = Instantiate(_attackModulePrefab, transform.Find("Modules/Attack"));
        _attackModule.SetChimera(this);

        _inputManager = new ChimeraInputManager(this);
    }

    public void Hurt(AttackModule attack)
    {
        Debug.Log("Chimera " + _playerNumber + " hurt.");
    }
}
