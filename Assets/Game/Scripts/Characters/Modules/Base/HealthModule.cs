﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthModule : BaseModule
{
    [SerializeField]
    private float _health = 100;

    public float Health
    {
        get { return _health; }
        set
        {
            _health -= value;

            if (_health < 0) Health = 0;

            if (Health == 0)
            {
                // TODO: Die
            }
        }
    }
}