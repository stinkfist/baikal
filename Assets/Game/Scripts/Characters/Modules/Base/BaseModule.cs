﻿using UnityEngine;

public class BaseModule : MonoBehaviour, IChimeraModule
{
    protected Chimera _chimera;

    public virtual void SetChimera(Chimera c)
    {
        _chimera = c;
    }
}
