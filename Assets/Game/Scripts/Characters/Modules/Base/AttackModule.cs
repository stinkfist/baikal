﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AttackModule : BaseModule
{
    [SerializeField]
    protected float _cooldown;

    protected float _lastAttackTime;
    protected bool CanAttack { get { return Time.time - _lastAttackTime >= _cooldown; } }

    protected virtual void FixedUpdate()
    {
        if (CanAttack && _chimera.InputManager.AttackButtonJustPressed)
            Attack();
    }

    private void Attack()
    { 
        PerformAttack();

        _lastAttackTime = Time.time;
    }

    protected abstract void PerformAttack();
}