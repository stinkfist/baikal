﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IChimeraModule
{
    void SetChimera(Chimera c);
}
