﻿using UnityEngine;

public class MovementModule : BaseModule
{
    [SerializeField]
    private float _xMoveSpeed;

    protected Rigidbody2D _rigidbody;

    public override void SetChimera(Chimera c)
    {
        base.SetChimera(c);

        _rigidbody = c.GetComponent<Rigidbody2D>();
    }

    protected virtual void FixedUpdate()
    {
        if (!_chimera.CanMove) return;

        _rigidbody.velocity = new Vector2(_xMoveSpeed * _chimera.InputManager.HorizontalAxisValue, _rigidbody.velocity.y);

        bool wasFacingRight = _chimera.IsFacingRight;

        if (!wasFacingRight && _rigidbody.velocity.x > 0 || wasFacingRight && _rigidbody.velocity.x < 0)
            _chimera.IsFacingRight = !_chimera.IsFacingRight;
    }
}