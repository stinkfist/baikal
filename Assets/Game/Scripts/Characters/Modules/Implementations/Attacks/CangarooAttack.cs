﻿using DG.Tweening;
using UnityEngine;

public class CangarooAttack : AttackModule
{
    [SerializeField]
    private float _range;

    [SerializeField]
    private float _attackTime;

    private Transform _leftFist;
    private Transform _rightFist;

    private void Start()
    {
        _leftFist = transform.Find("LeftFist");
        _rightFist = transform.Find("RightFist");

        HurtPlayerOnCollisionBehavior leftFistBehavior = _leftFist.GetComponent<HurtPlayerOnCollisionBehavior>();
        leftFistBehavior.SetAttack(this);
        leftFistBehavior.SetOwner(_chimera);

        HurtPlayerOnCollisionBehavior rightFistBehavior = _rightFist.GetComponent<HurtPlayerOnCollisionBehavior>();
        rightFistBehavior.SetAttack(this);
        rightFistBehavior.SetOwner(_chimera);
    }

    protected override void PerformAttack()
    {
        if (_chimera.IsFacingRight)
        {
            float _initialPosition = _rightFist.localPosition.x;
            _rightFist.DOLocalMoveX(_initialPosition + _range, _attackTime).SetEase(Ease.OutBack).OnComplete(
                () =>
                {
                    _rightFist.DOLocalMoveX(_initialPosition, _attackTime * 0.25f);
                }
            );
        }
        else
        {
            float _initialPosition = _leftFist.localPosition.x;
            _leftFist.DOLocalMoveX(_initialPosition - _range, _attackTime).SetEase(Ease.OutBack).OnComplete(
                () =>
                {
                    _leftFist.DOLocalMoveX(_initialPosition, _attackTime * 0.25f);
                }
            );
        }
    }
}
