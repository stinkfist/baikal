﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquirrelAttack : AttackModule
{
    [SerializeField]
    private GameObject _nutPrefab;

    [SerializeField]
    private float _throwForce;

    private Transform _leftSpawnpoint;
    private Transform _rightSpawnpoint;

    private void Awake()
    {
        _leftSpawnpoint = transform.Find("LeftSpawnpoint");
        _rightSpawnpoint = transform.Find("RightSpawnpoint");
    }

    protected override void PerformAttack()
    {
        Transform nutSpawnpoint = _chimera.IsFacingRight ? _rightSpawnpoint : _leftSpawnpoint;

        HurtPlayerOnCollisionBehavior newNut = Instantiate(_nutPrefab, nutSpawnpoint.position, Quaternion.identity).GetComponent<HurtPlayerOnCollisionBehavior>();
        newNut.SetAttack(this);
        newNut.SetOwner(_chimera);

        newNut.transform.right = nutSpawnpoint.right;
        newNut.GetComponent<Rigidbody2D>().velocity = _chimera.GetComponent<Rigidbody2D>().velocity;
        newNut.GetComponent<Rigidbody2D>().AddForce(newNut.transform.right * _throwForce);
    }
}
