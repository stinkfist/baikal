﻿using DG.Tweening;
using UnityEngine;

public class OwlAttack : AttackModule
{
    [SerializeField]
    private float _range;

    [SerializeField]
    private float _attackTime;

    private Transform _leftCollider;
    private Transform _rightCollider;

    private bool _isAttacking = false;

    private void Start()
    {
        _rightCollider = transform.Find("RightCollider");
        _leftCollider = transform.Find("LeftCollider");

        HurtPlayerOnCollisionBehavior leftColliderBehavior = _leftCollider.GetComponent<HurtPlayerOnCollisionBehavior>();
        leftColliderBehavior.SetAttack(this);
        leftColliderBehavior.SetOwner(_chimera);

        HurtPlayerOnCollisionBehavior rightColliderBehavior = _rightCollider.GetComponent<HurtPlayerOnCollisionBehavior>();
        rightColliderBehavior.SetAttack(this);
        rightColliderBehavior.SetOwner(_chimera);

        _rightCollider.gameObject.SetActive(false);
        _leftCollider.gameObject.SetActive(false);
    }

    protected override void PerformAttack()
    {
        if (_isAttacking) return;

        _isAttacking = true;

        float direction = 0;
        Transform activeCollider;

        if (_chimera.IsFacingRight)
        {
            activeCollider = _rightCollider;
            direction = 1;
        }
        else
        {
            activeCollider = _leftCollider;
            direction = -1;
        }

        activeCollider.gameObject.SetActive(true);
        _chimera.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        _chimera.transform.DOLocalMoveX(transform.position.x + _range * direction, _attackTime).SetEase(Ease.OutQuint).OnComplete(
            () =>
            {
                activeCollider.gameObject.SetActive(false);
                _isAttacking = false;
            }
        );
    }
}
