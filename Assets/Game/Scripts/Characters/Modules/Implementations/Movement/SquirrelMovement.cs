﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquirrelMovement : MovementModule
{
    [SerializeField]
    private float _jumpForce;

    [SerializeField]
    private bool CanJump { get { return _chimera.BottomCollider.IsTouchingLayers(1 << LayerMask.NameToLayer("Floor")); } }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if (CanJump && _chimera.InputManager.JumpButtonJustPressed)
        {
            _rigidbody.AddForce(Vector3.up * _jumpForce);
        }
    }
}
