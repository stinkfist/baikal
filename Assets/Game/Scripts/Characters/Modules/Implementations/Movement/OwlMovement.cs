﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwlMovement : MovementModule
{
    [SerializeField]
    private float _jumpForce;

    [SerializeField]
    private float _cooldown;
    private bool CanFly { get { return Time.time - _lastFlyTime >= _cooldown; } }
    private float _lastFlyTime;

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if (CanFly && _chimera.InputManager.JumpButtonJustPressed)
        {
            _rigidbody.velocity = Vector2.right * _rigidbody.velocity;
            _rigidbody.AddForce(Vector3.up * _jumpForce);

            _lastFlyTime = Time.time;
        }
    }
}
