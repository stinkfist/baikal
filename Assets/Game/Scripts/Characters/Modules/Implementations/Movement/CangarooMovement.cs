﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CangarooMovement : MovementModule
{
    [SerializeField]
    private float _jumpForce;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer("Floor")) return;

        _rigidbody.AddForce(Vector3.up * _jumpForce);
    }
}
