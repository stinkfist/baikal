﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayerOnCollisionBehavior : MonoBehaviour
{
    private AttackModule _attackModule;
    private Chimera _owner;

    public void SetAttack(AttackModule a)
    {
        _attackModule = a;
    }

    public void SetOwner(Chimera c)
    {
        _owner = c;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Chimera other = collision.gameObject.GetComponent<Chimera>();
        if (other == _owner) return;

        other.Hurt(_attackModule);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        Chimera other = collider.gameObject.GetComponent<Chimera>();
        if (other == _owner) return;

        other.Hurt(_attackModule);
    }
}
